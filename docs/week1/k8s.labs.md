## Kubernetes Labs


### vscode

Make sure you have installed this [extension](https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools) in your vscode.

!!! note
    I provde also a vscode settings with the recommended extensions, so make sure you installed those.

### minikube

Make sure you are in context `minikube` and that it is started.

```bash
minikube start

kubectl config use-context minikube

## you can work everything under the src/sre/k8s folder
```



#### Create namespace called `labs` (labs-ns.yaml) and store it here.

Here's an example copy/paste and edit to call it "labs"

!!! Example
    ```yaml
    apiVersion: v1
    kind: Namespace
    metadata:
        name:  name
    ```

once you have the file apply.

```kubectl apply -f labs-ns.yaml```

---

--8<--
src/sre/k8s/labs/README.md
--8<--

---

# Lab 1

--8<--
src/sre/k8s/labs/lab1.md
--8<--

---

# Lab 2

--8<--
src/sre/k8s/labs/lab2.md
--8<--

---

