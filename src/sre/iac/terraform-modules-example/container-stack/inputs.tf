variable "image_name" {
  description = "input image name to use"
  default = "nginx:latest"
}

variable "container_count" {
  type= number
  description = "input container count to use"
  default = 1
}
