"""server.py file"""
# pylint: disable=E0401
#!/usr/bin/env python3
import os
import platform
import collections
from datetime import datetime
from functools import wraps, update_wrapper

import redis
from flask import Flask, make_response, render_template

if os.getenv("OTEL_EXPORTER_OTLP_HEADERS", None):
    print("Enabling OTEL")
    from opentelemetry import trace
    from opentelemetry.instrumentation.flask import FlaskInstrumentor
    from opentelemetry.instrumentation.requests import RequestsInstrumentor
    from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter
    from opentelemetry.sdk.trace import TracerProvider
    from opentelemetry.sdk.trace.export import BatchSpanProcessor

    # Initialize tracing and an exporter that can send data to Honeycomb
    provider = TracerProvider()
    processor = BatchSpanProcessor(OTLPSpanExporter())
    provider.add_span_processor(processor)
    trace.set_tracer_provider(provider)
    tracer = trace.get_tracer(__name__)


app = Flask(__name__)

if os.getenv("OTEL_EXPORTER_OTLP_HEADERS", None):
    FlaskInstrumentor().instrument_app(app)
    RequestsInstrumentor().instrument()


app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0

REDIS_HOST = os.getenv("REDIS_HOST", "redis")
hostname = platform.node()
redisClient = redis.Redis(
    host=REDIS_HOST, port=6379, charset="utf-8", decode_responses=True
)


def nocache(view):
    """nocache does stuff"""
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers["Last-Modified"] = datetime.now()
        response.headers[
            "Cache-Control"
        ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "-1"
        return response

    return update_wrapper(no_cache, view)


def get_all():
    """get_all does stuff"""
    retries = 5
    all_items = {}
    while True:
        try:
            for key in redisClient.scan_iter("micro:*"):
                value = redisClient.hgetall(key)
                key = str(key).replace("micro:", "")
                all_items[key] = value
                print(f"get all: {key} = {value}")
            print(all_items)
            ordered_items = collections.OrderedDict(
                sorted(all_items.items(), key=lambda item: item[1]["visits"])
            )
            return ordered_items
        except redis.exceptions.ConnectionError as _exc:
            if retries == 0:
                return "'No Redis Connection'"
            retries -= 1


def get_hostname_count():
    """get_hostname_count does stuff"""
    retries = 5
    while True:
        try:
            value = redisClient.incr(f"app:{hostname}")
            redisClient.expire(name=f"app:{hostname}", time=15)
            return value
        except redis.exceptions.ConnectionError as _exc:
            if retries == 0:
                return "'No Redis Connection'"
            retries -= 1


@app.route("/")
@nocache
def hello():
    """hello endpoint"""
    all_items = get_all()
    print(all_items)
    return render_template(
        "index.html",
        all_items=all_items,
        hostname_count=get_hostname_count(),
        WELCOME_MSG=os.getenv("WELCOME_MSG", "Welcome"),
        MICRO_URL=os.getenv("MICRO_URL", "http://localhost:8080"),
    )


@app.route("/health")
def health():
    """health endpoint"""
    return "Im healthy"


if __name__ == "__main__":
    app.run(host="0.0.0.0")
